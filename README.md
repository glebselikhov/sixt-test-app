# Selikhov Hlib Sixt-test-app

In the project directory, you can run:

### `yarn`

and then

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### Some words about chose libraries

This project was bootstrapped with Create-React-App and following libraries such as `react` `react-dom` `axios` etc

Also for some visual stuff I've chose these libraries
`node-sass` - for styling
`react-json-view` - component for displaying JSON
`react-spinners` - displaying loaders

As a state managing part of the app, I've chosen `redux` and
`redux-thunk` - async store logic
`react-redux` - official React binding for Redux
