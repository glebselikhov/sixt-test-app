import axios from "axios";
import fakeData from "data/offers.json";
import { AppThunk } from "types/general";
import { fetchCars, fetchCar, fetchCarFromList } from "actions/carsActions";
import { Car } from "actions/types";

export const getCars = (id?: string): AppThunk => async (dispatch) => {
  //TODO: get API url from env variables
  axios
    .get("http://cdn.sixt.io/codingtask/offers.json")
    .then(async ({ data }) => {
      if (id) {
        await dispatch(fetchCars(data));
        await dispatch(fetchCarFromList(id));
      } else {
        dispatch(fetchCars(data));
      }
    })
    .catch((err) => {
      dispatch(fetchCars(fakeData));
    });
};

export const getCar = (id: string, list: Car[]): AppThunk => async (
  dispatch
) => {
  // Fake one entity request
  axios
    .get(`http://cdn.sixt.io/codingtask/${id}/offers.json`)
    .then(({ data }) => {
      dispatch(fetchCar(data));
    })
    .catch((err) => {
      if (list.length) {
        dispatch(fetchCarFromList(id));
      } else {
        dispatch(getCars(id));
      }
    });
};
