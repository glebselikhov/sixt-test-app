import { combineReducers } from 'redux';
import {carsReducer} from './carsReducer';

const rootReducer = combineReducers({
    cars: carsReducer,
})

export type AppState = ReturnType<typeof rootReducer>
export default rootReducer;