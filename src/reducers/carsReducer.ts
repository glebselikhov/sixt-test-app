import {
  FETCH_CARS,
  FETCH_CAR,
  FETCH_CAR_FROM_LSIT,
  Car,
  CarsActionTypes,
} from "actions/types";

interface CarsState {
  list: Car[];
  data?: Car;
  info?: any;
}

export const initialState: CarsState = {
  list: [] as Car[],
  data: {} as Car,
};

export function carsReducer(
  state = initialState,
  action: CarsActionTypes
): CarsState {
  switch (action.type) {
    case FETCH_CARS:
      return {
        ...state,
        list: [...action.payload.offers],
        info: action.payload.info,
      };
    case FETCH_CAR:
      return {
        ...state,
        data: action.payload,
      };
    case FETCH_CAR_FROM_LSIT:
      return {
        ...state,
        data: state.list.find((el) => el.id === action.payload),
      };
    default:
      return state;
  }
}
