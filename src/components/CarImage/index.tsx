import React, { FunctionComponent } from "react";
import { imageUrlAdapter } from "utils/adapters";
import { Images } from "types/general";

interface ImageProps {
  img: Images | undefined;
  className: string;
  fallBackImg?: any;
}

const CarImage: FunctionComponent<ImageProps> = ({
  img,
  className,
  fallBackImg,
}) => {
  const src = img ? imageUrlAdapter(img && img.medium) : fallBackImg;
  return (
    <div className="car-img-wrapper">
      <img className={className} src={src} alt="Car" />
    </div>
  );
};

export default CarImage;
