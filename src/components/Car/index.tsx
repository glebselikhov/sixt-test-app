import CarImage from "components/CarImage";
import React, { FunctionComponent } from "react";
import { Link } from "react-router-dom";
import { Headlines, Images, Prices } from "types/general";
import { routesGetter } from "utils/constants";
import fallBackImg from "assets/fallBack-Car-Img.jpg";

import styles from "./index.module.scss";

interface CarProps {
  id: string;
  img: Images;
  price: Prices;
  heading: Headlines;
  description: string;
}

const Car: FunctionComponent<CarProps> = ({
  img,
  heading,
  price,
  description,
  id,
}) => {
  return (
    <div className={styles.car}>
      <div className={styles.heading}>{heading.description}</div>

      <div className={styles.imgWrapper}>
        <CarImage className={styles.img} img={img} fallBackImg={fallBackImg} />
      </div>
      <div className={styles.contentWrapper}>
        <div className={styles.description}>{description}</div>
        <div className={styles.price}>
          {`${price.basePrice.amount.display} ${price.basePrice.amount.currency}`}
        </div>
        <Link to={() => routesGetter.getCarInfo(id)} className={styles.btn}>
          Show more info
        </Link>
      </div>
    </div>
  );
};

export default Car;
