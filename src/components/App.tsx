import React from 'react';
import MainRouter from './routes';
import Header from 'components/Header';
import Footer from 'components/Footer';

import 'styles/global.scss'

function App() {
  return (
    <div className="App">
      <Header/>
      <section className="content-wrapper">
        <MainRouter/>
      </section>
      <Footer/>
    </div>
  );
}

export default App;
