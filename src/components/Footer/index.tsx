import React from "react";

import "./index.module.scss";

export default function Footer() {
  return (
    <footer>
      <div className="bottomInfo">
        <div className="info">Copyright © 2021–2021 Gleb Selikhov </div>
      </div>
    </footer>
  );
}
