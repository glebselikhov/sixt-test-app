import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { CarInfo, Catalog } from "pages";
import { routes } from "utils/constants";

const MainRouter = () => {
  return (
    <Router>
      <Switch>
        <Route exact path={routes.main}>
          <Catalog />
        </Route>
        <Route path={routes.carInfo}>
          <CarInfo />
        </Route>
      </Switch>
    </Router>
  );
};

export default MainRouter;
