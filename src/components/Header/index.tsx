import React from 'react';

import './index.module.scss';

export default function Header() {
    return (
        <header>
            <h1>Gleb Selikhov Test App</h1>
        </header>
    )
}
