import { ThunkAction } from "redux-thunk";
import { Action } from "redux";
import { AppState } from "reducers";

export interface Images {
  large: string;
  medium: string;
  small: string;
}

interface onePriceInfo {
  amount: {
    currency: string;
    display: string;
    value: number;
  };
  prefix: string;
  taxInfo: string;
  tracking: number;
  trackingNet: number;
  unit: string;
}

export interface Prices {
  totalPrice: onePriceInfo;
  dayPrice: onePriceInfo;
  basePrice: onePriceInfo;
}

export interface Headlines {
    description: string;
    longSubline: string;
    shortSubline: string;
}

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  AppState,
  unknown,
  Action<string>
>;
