import { Images, Prices, Headlines } from "types/general";
export const FETCH_CAR = "FETCH_CAR";
export const FETCH_CARS = "FETCH_CARS";
export const FETCH_CAR_FROM_LSIT = "FETCH_CAR_FROM_LSIT";

interface fetchCarsAction {
  type: typeof FETCH_CARS;
  payload: CarsInfo;
}

interface fetchCarAction {
  type: typeof FETCH_CAR;
  payload: Car;
}

interface fetchCarFromList {
  type: typeof FETCH_CAR_FROM_LSIT;
  payload: string,
}

export interface Car {
  headlines: Headlines
  description: string;
  images: Images;
  prices: Prices;
  id: string;
}

export interface CarsInfo {
  info: any;
  offers: Car[];
  recommendations?: any;
  recommendations_v2?: any;
}

export type CarsActionTypes = fetchCarsAction | fetchCarAction | fetchCarFromList;
