import {FETCH_CARS, FETCH_CAR, FETCH_CAR_FROM_LSIT, CarsActionTypes, CarsInfo, Car} from './types';

export function fetchCars(newCars: CarsInfo): CarsActionTypes {
    return {
      type: FETCH_CARS,
      payload: newCars
    }
}

export function fetchCar(oneCar: Car): CarsActionTypes {
  return {
    type: FETCH_CAR,
    payload: oneCar
  }
} 

export function fetchCarFromList(id: string): CarsActionTypes {
  return {
    type: FETCH_CAR_FROM_LSIT,
    payload: id,
  }
} 