import { carsReducer, initialState } from "reducers/carsReducer";
import expect from "expect";
import { CarsActionTypes } from "actions/types";
import { fetchCars } from "actions/carsActions";
import { allCars, allCarsStateExpected } from "../mock/carsReducerMock";

describe("Cars reducer", () => {
  it("should return the initial state", () => {
    expect(carsReducer(initialState, {} as CarsActionTypes)).toEqual({
      list: [],
      data: {},
    });
  });
  it("should handle FETCH_CARS", () => {
    expect(carsReducer(initialState, fetchCars(allCars))).toEqual(
      allCarsStateExpected
    );
  });
  // it('should handle FETCH_CAR');
  // it('should handle FETCH_CAR_FROM_LSIT');
});
