export const imageUrlAdapter = (url: string | undefined): string => {
  const replacedUrl = url ? url.replace("//", "/") : ''
  return `https://sixt.com${replacedUrl}`;
};
