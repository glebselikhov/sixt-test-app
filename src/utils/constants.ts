interface ObjectLiteral<T> {
  [key: string]: T;
}

export const routes: ObjectLiteral<string> = {
  main: "/",
  carInfo: "/car/:carId",
};

export const routesGetter: ObjectLiteral<(id: string) => string> = {
  getCarInfo: (id: string): string => `/car/${id}`,
}
