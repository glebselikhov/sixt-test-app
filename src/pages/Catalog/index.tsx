import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { AppState } from "reducers";
import { getCars } from "api/cars";

import Car from "components/Car";

import "styles/carsPage.scss";

const Catalog = () => {
  const dispatch = useDispatch();
  const list = useSelector((state: AppState) => state.cars.list);

  useEffect(() => {
    dispatch(getCars());
  }, [dispatch]);

  return (
    <div className="cars-wrapper">
      {list.map((el) => (
        <Car
          key={el.id}
          price={el.prices}
          heading={el.headlines}
          description={el.description}
          img={el.images}
          id={el.id}
        />
      ))}
    </div>
  );
};

export default Catalog;
