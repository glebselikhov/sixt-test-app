import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import ReactJson from "react-json-view";
import { useParams } from "react-router-dom";
import { ScaleLoader } from "react-spinners";

import { AppState } from "reducers";
import { getCar } from "api/cars";
import CarImage from "components/CarImage";
import fallBackImg from "assets/fallBack-Car-Img.jpg";


import "styles/carsPage.scss";
import styles from "./index.module.scss";

interface ParamTypes {
  carId: string;
}

const CarInfo = () => {
  const [showInfo, setShowInfo] = useState(true);
  const dispatch = useDispatch();
  const { carId } = useParams<ParamTypes>();
  const info = useSelector((state: AppState) => state.cars.data);
  const list = useSelector((state: AppState) => state.cars.list);

  useEffect(() => {
    setShowInfo(!!info && !!Object.keys(info).length);
  }, [info]);

  useEffect(() => {
    dispatch(getCar(carId, list));
  }, [dispatch, carId, list]);

  return (
    <div className={styles.carInfoWrapper}>
      {showInfo ? (
        <>
          <CarImage className={styles.carImg} img={info?.images} fallBackImg={fallBackImg}/>
          <ReactJson src={info as object} />
        </>
      ) : (
        <ScaleLoader />
      )}
    </div>
  );
};

export default CarInfo;
